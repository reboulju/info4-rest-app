# Spring Boot Rest Application

This project contains the java part of a multi-tier project to be deployed on a Kubernetes cluster.

The Gitlab CI is used to compile the project and to deploy it onto a cloud provider.

## Build

use gradle or the embedded `gradlew` file to build the project.

Generate the jar

```
gradle build
```

or 

```
./gradlew build
```

## Workshops - Prerequisite

Docker CE will be also used for this workshops. 
Docker images will be deployed into a cloud environment. 
As we cannot afford using a commercial Cloud offer, we will use the [`minikube`](https://kubernetes.io/fr/docs/setup/learning-environment/minikube/) environment.
To manage instances, deployments and services, `Kubernetes` is the de-facto technology to do that.
Install and use [`kubectl`](https://kubernetes.io/fr/docs/tasks/tools/install-kubectl/) locally to orchestrate all this.