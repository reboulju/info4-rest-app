FROM openjdk:11
COPY build/libs /usr/libs
WORKDIR /usr/libs
CMD ["java", "-jar", "spring-boot-0.0.1-SNAPSHOT.jar"]
